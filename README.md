# ffsky-gradle-plugin-template

#### 介绍
gradle-plugin插件模板，基于Android Studio IDE搭建，
如果你用的是【idea IDE】可能会打开失败、无法识别项目接口，可以把【lib_gradle_plugin】模块拷贝出去并搭建一个java项目。
1. lib_gradle_plugin_template:  gradle-plugin插件模板


### 引用
``
api 'com.gitee.hljdrl:xxx:xxx' 
 

#### 使用说明
1. 目前使用classpath "com.android.tools.build:gradle:4.0.2" 插件，升级过高版本会导致 apply plugin: 'maven' 插件找不到。
2. 组件开发本地发布后，如果需要命令行测试，需要把【lib_gradle_plugin】模块从 settings.gradle中注释掉，不然会出现奇怪问题。
3. 组件编译发布异常，也需要将 build.gradle中  classpath "com.gitee.hljdrl:gradle:1.0.0-SNAPSHOT" 注释掉，
    app模块的引用也需要注释掉 app/build.gradle apply plugin: 'com.gitee.hljdrl.gradle' 和 MyTaskConfig{} 配置。


<img src="art/build.png" />


<img src="art/import_config.png" />
