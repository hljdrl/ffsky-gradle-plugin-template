package com.gitee.hljdrl.plugin;

import com.android.build.gradle.AppExtension;
import com.android.build.gradle.AppPlugin;
import com.gitee.hljdrl.plugin.ext.MyExtension;
import com.gitee.hljdrl.plugin.logger.FFLog;
import com.gitee.hljdrl.plugin.task.MyTask;
import com.gitee.hljdrl.plugin.transform.FFSkyTransform;
import com.gitee.hljdrl.plugin.transform.ReadTransform;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.Collections;

public class FFSkyPlugin implements Plugin<Project> {

    public static final boolean DEBUG = true;
    @Override
    public void apply(Project project) {
        project.getTasks().create("MyTaskName", MyTask.class);
        //-----------------------------------------------------------------
        project.getExtensions().create("MyTaskConfig", MyExtension.class);

        boolean hasPlugin = project.getPlugins().hasPlugin(AppPlugin.class);
        if(hasPlugin){
            FFLog.log(">> RegisterTransform ");
            AppExtension app = project.getExtensions().findByType(AppExtension.class);
//            app.registerTransform(new ReadTransform(), Collections.EMPTY_LIST);
            app.registerTransform(new FFSkyTransform(), Collections.EMPTY_LIST);
        }
    }
}
