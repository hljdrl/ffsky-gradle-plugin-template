package com.gitee.hljdrl.plugin.logger;

public class FFLog {


    public static void log(String tag) {
        logout(tag,"");
    }

    public static void log(String tag, String msg) {
        logout(tag,msg);
    }

    public static void log(String tag, String... msg) {
        logout(tag,toString(msg));
    }



    public static void log(String tag, Object... msg) {
        logout(tag,toString(msg));
    }

    private static void logout(String tag,String msg){
        System.out.println(toString(tag,msg));
    }

    public static void error(String tag) {
        errLogout(tag,"");
    }

    public static void error(String tag, String msg) {
        errLogout(tag,msg);
    }

    public static void error(String tag, String... msg) {
        errLogout(tag,toString(msg));
    }

    public static void error(String tag, Object... msg) {
        errLogout(tag,toString(msg));
    }

    private static void errLogout(String tag,String msg){
        System.out.println(toString(tag,msg));
    }


    protected static String toString(String... list) {
        StringBuffer buf = new StringBuffer();
        for (String item : list) {
            buf.append(item);
        }
        return buf.toString();
    }
    protected static String toString(Object... list) {
        StringBuffer buf = new StringBuffer();
        for (Object item : list) {
            if(item!=null) {
                buf.append(item.toString());
            }else{
                buf.append(" NULL ");
            }
        }
        return buf.toString();
    }
}
