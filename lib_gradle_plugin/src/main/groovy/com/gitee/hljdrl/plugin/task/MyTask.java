package com.gitee.hljdrl.plugin.task;

import com.gitee.hljdrl.plugin.basic.BasicMyTask;
import com.gitee.hljdrl.plugin.ext.MyExtension;
import com.gitee.hljdrl.plugin.logger.FFLog;

import org.gradle.api.Project;
import org.gradle.api.tasks.TaskAction;

public class MyTask extends BasicMyTask {


    @TaskAction
    public void doTaskAction() {
        loggerSTART();
        Project project = getProject();
        loggerTask("  > ");
        loggerTask("  > name=",project.getName());
        loggerTask("  > dir=",project.getProjectDir().toString());
        loggerTask("  > rootProject dir=",project.getRootProject().getProjectDir().toString());
        loggerTask("  > ");
        MyExtension extension = getProject().getExtensions().getByType(MyExtension.class);
        if (extension == null) {
            FFLog.error("> Task  > Error: ", "缺少MyExtension{ }配置项!!!");
            FFLog.error("> Task  > Error: ", "\nMyExtension{ \n path=? \n appKey=? \n suffix=? \n}");
            loggerFAIL();
            return;
        } else {
            if (extension.path == null || extension.appKey == null || extension.suffix == null) {
                FFLog.error("> Task  > Error: ", "缺少MyExtension{ }配置项!!!");
                FFLog.error("> Task  > Error: ", "\nMyExtension{ \n path=? \n appKey=? \n suffix=? \n}");
                loggerFAIL();
                return;
            }
        }
        loggerTask("  > ");
        loggerTask("  > gradle plugin 插件模板");
        loggerTask("  > ");
        loggerTask("  > ");
        loggerTask("finish");
        loggerSUCCESSFUL();
    }
}
