package com.gitee.hljdrl.plugin.transform;


import com.android.build.api.transform.Context;
import com.android.build.api.transform.DirectoryInput;
import com.android.build.api.transform.Format;
import com.android.build.api.transform.JarInput;
import com.android.build.api.transform.QualifiedContent;
import com.android.build.api.transform.Transform;
import com.android.build.api.transform.TransformException;
import com.android.build.api.transform.TransformInput;
import com.android.build.api.transform.TransformOutputProvider;
import com.android.build.gradle.internal.pipeline.TransformManager;
import com.gitee.hljdrl.plugin.logger.FFLog;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class FFSkyTransform extends Transform {
    @Override
    public String getName() {
        return "ffSky";
    }

    @Override
    public Set<? super QualifiedContent.Scope> getScopes() {
        return TransformManager.SCOPE_FULL_PROJECT;
    }
    @Override
    public Set<QualifiedContent.ContentType> getInputTypes() {
        return TransformManager.CONTENT_CLASS;
    }
    @Override
    public boolean isIncremental() {
        return false;
    }

    @Override
    public void transform(Context context, Collection<TransformInput> inputs, Collection<TransformInput> referencedInputs, TransformOutputProvider outputProvider, boolean isIncremental) throws IOException, TransformException, InterruptedException {
        if (!isIncremental()) {
            outputProvider.deleteAll();
        }
        for (TransformInput input : inputs) {
            //返回的是ImmutableJarInput
            for (JarInput jarInput : input.getJarInputs()) {
                FFLog.error("jar file = " , jarInput.getFile());
                String destName = jarInput.getName();
                // rename jar files
                String hexName = DigestUtils.md5Hex(jarInput.getFile().getAbsolutePath());

                if (destName.endsWith(".jar")) {
                    destName = destName.substring(0, destName.length() - 4);
                }
                // input file
                File src = jarInput.getFile();
                // output file
                File dest = outputProvider.getContentLocation(destName + "_" + hexName, jarInput.getContentTypes(), jarInput.getScopes(), Format.JAR);
                //***************************************************
//                ScanUtil.scanJar(src, dest, mTransformFile);
                //***************************************************
                FileUtils.copyFile(src, dest);
            }
            //返回的是ImmutableDirectoryInput
            for (DirectoryInput directoryInput : input.getDirectoryInputs()) {
                FFLog.error("directory file = " , directoryInput.getFile());
                File dest = outputProvider.getContentLocation(directoryInput.getName(), directoryInput.getContentTypes(),
                        directoryInput.getScopes(), Format.DIRECTORY);
                File dir = directoryInput.getFile();
                //***************************************************
                Collection<File> files = FileUtils.listFiles(dir, new String[]{"class"}, true);
                for (File itemFile : files) {
//                    ScanUtil.scanClass(dir, itemFile, mTransformFile);
                }
                //***************************************************
                FileUtils.copyDirectory(directoryInput.getFile(), dest);
            }
        }


    }
}
