package com.gitee.hljdrl.plugin.util;

public class StringUtil {

    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }


    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static boolean equals(CharSequence a, CharSequence b) {
        if (a == b) return true;
        int length;
        if (a != null && b != null && (length = a.length()) == b.length()) {
            if (a instanceof String && b instanceof String) {
                return a.equals(b);
            } else {
                for (int i = 0; i < length; i++) {
                    if (a.charAt(i) != b.charAt(i)) return false;
                }
                return true;
            }
        }
        return false;
    }

    public static String string(String... str) {
        StringBuffer _buf = new StringBuffer();
        if (str != null) {
            for (String _s : str) {
                _buf.append(_s);
            }
        }
        return _buf.toString();
    }

    public static String string(Object... str) {
        StringBuffer _buf = new StringBuffer();
        if (str != null) {
            for (Object _s : str) {
                if (_s != null) {
                    _buf.append(_s.toString());
                }
            }
        }
        return _buf.toString();
    }


}
