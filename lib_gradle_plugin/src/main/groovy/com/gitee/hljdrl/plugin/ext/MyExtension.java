package com.gitee.hljdrl.plugin.ext;

/**
 * 华为OBS上传apk配置信息
 * dai.rui.lin
 *
 */
public class MyExtension {

    /**
     * 文档本地路径
     */
    public String path;

    /**
     * 文件后缀: .apk , .md  , .html
     */
    public String suffix[];

    public String appKey;

}
