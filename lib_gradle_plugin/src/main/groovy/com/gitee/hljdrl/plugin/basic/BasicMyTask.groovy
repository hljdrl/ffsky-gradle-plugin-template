package com.gitee.hljdrl.plugin.basic


import com.gitee.hljdrl.plugin.logger.FFLog
import org.gradle.api.DefaultTask

public class BasicMyTask extends DefaultTask {


    public BasicMyTask(){
        setGroup(TaskEvent.GROUP_NAME);
    }

    static void loggerSUCCESSFUL(){
        FFLog.error("> Task :{}: SUCCESSFUL","")
        FFLog.error("", "")
    }
    static void loggerTask(String message){
        FFLog.error("> Task :{}: ", message)
    }

    static void loggerTask(String... message){
        FFLog.error("> Task :{}: ", message)
    }

    static void loggerSTART(){
        FFLog.error("> Task  > START")
    }

    static void loggerFAIL(){
        FFLog.error("> Task  > FAIL")
    }

}