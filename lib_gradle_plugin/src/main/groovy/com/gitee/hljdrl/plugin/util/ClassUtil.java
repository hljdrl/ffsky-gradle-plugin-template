package com.gitee.hljdrl.plugin.util;

public class ClassUtil {

    public static  String formatClassName(String className) {
        return className.replaceAll("/", ".").replaceAll("\\\\", ".");
    }

    public static  String formatClassName(String root,String className) {
        if(StringUtil.isEmpty(root)){
            return className.replaceAll("/", ".").replaceAll("\\\\", ".");
        }
        String ret = className.substring(root.length() );
        return ret.replaceAll("/", ".").replaceAll("\\\\", ".");
    }
}
